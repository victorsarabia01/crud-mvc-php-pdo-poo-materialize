-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-08-2022 a las 22:26:43
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `zapato`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dboestilo`
--

CREATE TABLE `dboestilo` (
  `id_estilo` int(11) NOT NULL,
  `estilo` varchar(45) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `dboestilo`
--

INSERT INTO `dboestilo` (`id_estilo`, `estilo`) VALUES
(1, 'casual'),
(2, 'deportivo'),
(3, 'colegial');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dbogenero`
--

CREATE TABLE `dbogenero` (
  `id_genero` int(11) NOT NULL,
  `genero` varchar(45) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `dbogenero`
--

INSERT INTO `dbogenero` (`id_genero`, `genero`) VALUES
(1, 'masculino'),
(2, 'femenino');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dbotalla`
--

CREATE TABLE `dbotalla` (
  `id_talla` int(11) NOT NULL,
  `talla` varchar(45) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `dbotalla`
--

INSERT INTO `dbotalla` (`id_talla`, `talla`) VALUES
(1, '6'),
(2, '7'),
(3, '8'),
(4, '9');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dbozapato`
--

CREATE TABLE `dbozapato` (
  `id_zapato` int(11) NOT NULL,
  `precio` decimal(18,2) NOT NULL,
  `color` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `id_estilo` int(11) NOT NULL,
  `id_talla` int(11) NOT NULL,
  `id_genero` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `dbozapato`
--

INSERT INTO `dbozapato` (`id_zapato`, `precio`, `color`, `id_estilo`, `id_talla`, `id_genero`, `cantidad`) VALUES
(1, '35.00', 'rojo', 1, 1, 1, 45),
(2, '40.00', 'negro', 3, 3, 2, 45),
(4, '35.00', 'rojo', 1, 4, 1, 5),
(6, '1.00', 'red', 2, 4, 2, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `dboestilo`
--
ALTER TABLE `dboestilo`
  ADD PRIMARY KEY (`id_estilo`);

--
-- Indices de la tabla `dbogenero`
--
ALTER TABLE `dbogenero`
  ADD PRIMARY KEY (`id_genero`);

--
-- Indices de la tabla `dbotalla`
--
ALTER TABLE `dbotalla`
  ADD PRIMARY KEY (`id_talla`);

--
-- Indices de la tabla `dbozapato`
--
ALTER TABLE `dbozapato`
  ADD PRIMARY KEY (`id_zapato`),
  ADD KEY `id_estilo` (`id_estilo`),
  ADD KEY `id_talla` (`id_talla`),
  ADD KEY `id_genero` (`id_genero`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `dboestilo`
--
ALTER TABLE `dboestilo`
  MODIFY `id_estilo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `dbogenero`
--
ALTER TABLE `dbogenero`
  MODIFY `id_genero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `dbotalla`
--
ALTER TABLE `dbotalla`
  MODIFY `id_talla` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `dbozapato`
--
ALTER TABLE `dbozapato`
  MODIFY `id_zapato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `dbozapato`
--
ALTER TABLE `dbozapato`
  ADD CONSTRAINT `fk1` FOREIGN KEY (`id_estilo`) REFERENCES `dboestilo` (`id_estilo`),
  ADD CONSTRAINT `fk2` FOREIGN KEY (`id_genero`) REFERENCES `dbogenero` (`id_genero`),
  ADD CONSTRAINT `fk3` FOREIGN KEY (`id_talla`) REFERENCES `dbotalla` (`id_talla`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

    class conexion{
        private $hostname = "localhost";
        private $database = "zapato";
        private $username = "root";
        private $password = "";
        private $charset = "utf8";

        /*public $hostname = "localhost";
        public $database = "zapato";
        public $username = "root";
        public $password = "";
        public $charset = "utf8";*/


        function conectar(){
            try {
                /*$conexion = "mysql:host=".$this->hostname.";dbname=".$this->database.";charset=".$this->charset;*/
                $conexion = "mysql: host=localhost; dbname=zapato; charset=utf8";
                $options = [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_EMULATE_PREPARES => false,
                ];
                $pdo = new PDO ($conexion,"root","",$options);
                return $pdo;
            } catch (PDOexception $e) {
                echo 'Error conexion' . $e->getMessage();
                exit;
            }
        }
    }


?>
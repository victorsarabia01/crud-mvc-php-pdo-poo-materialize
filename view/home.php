<?php
	//include_once 'controller/control.php';
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="resources/css/materialize.css">
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col m12">
				<div class="card black white-text center-align">
					<h2>Lista de Zapatos</h2>
				</div>
				
			</div>
		</div>
		<div class="row">
			<div class="col m12">
				<table class="table-responsive z-depth-3">
					<tr class="black" >
						<th class="white-text center">ID</th>
						<th class="white-text center">Precio</th>
						<th class="white-text center">Color</th>
						<th class="white-text center">Estilo</th>
						<th class="white-text center">Talla</th>
						<th class="white-text center">Genero</th>
						<th class="white-text center">Cantidad</th>
						<th class="white-text center"></th>
						<th class="white-text center"></th>
					</tr>
					<?php foreach ($this->MODEL->mostrarDatos() as $k) : ?>
						<tr >
						<th class="text center"><?php echo $k->id_zapato; ?></th>
						<td class="text center">$<?php echo $k->precio; ?></td>
						<td class="text center"><?php echo $k->color; ?></td>
						<td class="text center"><?php echo $k->estilo; ?></td>
						<td class="text center"><?php echo $k->talla; ?></td>
						<td class="text center"><?php echo $k->genero; ?></td>
						<td class="text center"><?php echo $k->cantidad; ?></td>
						<td>
							<a href="?c=eliminar&id=<?php echo $k->id_zapato; ?>" class="btn red">Eliminar</a>
						</td>
						<td>
							<a href="?c=nuevo&id=<?php echo $k->id_zapato; ?>" class="btn blue">Modificar</a>
						</td>
						</tr>
					<?php endforeach ?>
				</table>
				
				<a href="?c=nuevo" class="btn btn-block green">Nuevo</a>
			</div>
		</div>
	</div>

</body>
</html>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="resources/css/materialize.css">
</head>
<body>

	<div class="container">
		<form method="post" action="?c=guardar">
			<div class="row">
			<div class="col m12">
				<div class="card black white-text center-align">
					<h2>Lista de Zapatos</h2>
				</div>
				
			</div>
		</div>
		<div class="row">
			<div class="col m3"></div>
			<div class="col m3">Precio: </div>
			<div class="col m3">
				<input type="hidden" name="txtID" value="<?php echo $alm->id_zapato; ?>">
				<input type="text" name="txtPrecio" value="<?php echo $alm->precio; ?>">
			</div>
		</div>
		<div class="row">
			<div class="col m3"></div>
			<div class="col m3">Color: </div>
			<div class="col m3">
				<input type="text" name="txtColor" value="<?php echo $alm->color; ?>">
				
			</div>
		</div>
		<div class="row">
			<div class="col m3"></div>
			<div class="col m3">Estilo: </div>
			<div class="col m3">
				<select name="selectEstilo">
					<?php foreach ($this->MODEL->cargarEstilo()  as $k) : ?>
						<option value="<?php echo $k->id_estilo ?>" <?php echo $k->id_estilo == $alm->id_estilo ? 'selected' : ''; ?>><?php echo $k->estilo ?></option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col m3"></div>
			<div class="col m3">Talla: </div>
			<div class="col m3">
				<select name="selectTalla">
					<?php foreach ($this->MODEL->cargarTallas()  as $k) : ?>
						<option value="<?php echo $k->id_talla ?>" <?php echo $k->id_talla == $alm->id_talla ? 'selected' : ''; ?>><?php echo $k->talla ?></option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col m3"></div>
			<div class="col m3">Genero: </div>
			<div class="col m3">
				<select name="selectGenero">
					<?php foreach ($this->MODEL->cargarGenero()  as $k) : ?>
						<option value="<?php echo $k->id_genero ?>" <?php echo $k->id_genero == $alm->id_genero ? 'selected' : ''; ?>><?php echo $k->genero ?></option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col m3"></div>
			<div class="col m3">Cantidad: </div>
			<div class="col m3">
				<input type="text" name="cantidad" value="<?php echo $alm->cantidad; ?>">
				
			</div>
		</div>
		<div class="row">
			<div class="col m3"></div>
		
			<div class="col m6">
				<input type="submit" name="ntym" value="Guardar" class="btn green">
			</div>
		</div>
		</form>
	</div>
	<script type="text/javascript" src="resources/js/Jquery.js"></script>
	<script type="text/javascript" src="resources/js/materialize.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('select').formSelect();
		});
	</script>
</body>
</html>
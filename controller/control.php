<?php
include_once 'model/zapato.php';
	class control{

		public $MODEl;

		public function __construct(){
			$this->MODEL = new zapato();
		}

		public function index(){
			include_once 'view/home.php';
		}
		public function nuevo(){
			$alm = new zapato();
			if(isset($_REQUEST['id'])){
				$alm = $this->MODEL->cargarID($_REQUEST['id']);
			}
			include_once 'view/save.php';
		}
		public function guardar(){
			$alm = new zapato();
			$alm->id_zapato = $_POST['txtID'];
			$alm->precio = $_POST['txtPrecio'];
			$alm->color = $_POST['txtColor'];
			$alm->id_estilo = $_POST['selectEstilo'];
			$alm->id_talla = $_POST['selectTalla'];
			$alm->id_genero = $_POST['selectGenero'];
			$alm->cantidad = $_POST['cantidad'];

			$alm->id_zapato > 0 ? $this->MODEL->actualizarDatos($alm) : $this->MODEL->registrar($alm);
			
			header("Location: index.php");
		}
		public function eliminar(){
			$this->MODEL->delete($_REQUEST['id']);
			header("Location: index.php");
		}
	}
?>